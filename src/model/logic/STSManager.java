package model.logic;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.data_structures.*;

import java.io.*;
import java.sql.Date;
import java.util.Calendar;

import javax.xml.bind.ParseConversionEvent;
import model.vo.*;

public class STSManager implements ISTSManager {

	private DoubleLinkedList<VORoute> routes;
	private RingList<VOTrip> trips;
	private DoubleLinkedList<VOStopTimes> stopTimes;
	private RingList<VOStop> stops;

	@Override
	public void loadRoutes(String routesFile){
		routes = new DoubleLinkedList<VORoute>();

		//Abre el archivo
		File file = new File(routesFile);
		BufferedReader in;
		try{
			in = new BufferedReader(new FileReader(file));
		}
		catch(Exception e){
			System.out.println("No se encontr� el archivo");
			return;
		}

		//Lee el archivo
		try{
			//Lee la linea de referencia
			String line = in.readLine();

			//Empieza desde la primera linea de contenido
			line = in.readLine();
			while(!line.equals(null)){
				line.trim();
				String content[] = line.split(",");
				for(int i=0; i<content.length; i++){
					content[i] = content[i].trim();
				}
				
				if(content.length != 0){
					//Se crea un objeto a partir del archivo para ser agregado a la lista
					VORoute actual = new VORoute(content[0], content[1], content[2], content[3], content[4],
							 Integer.parseInt(content[5]), content[6]);
					routes.add(actual);
				}
				
				line = in.readLine();
			}			
		}
		catch(IOException e){
			System.out.println("No se pudo leer el archivo");
		}

		//Cierra el lector
		try{
			in.close();
		}
		catch(IOException e){
			System.out.println("No se pudo cerrar el lector de archivos");
			return;
		}
	}

	@Override
	public void loadTrips(String tripsFile) {
		trips = new RingList<VOTrip>();

		//Abre el archivo
		File file = new File(tripsFile);
		BufferedReader in;
		try{
			in = new BufferedReader(new FileReader(file));
		}
		catch(Exception e){
			System.out.println("No se encontr� el archivo");
			return;
		}

		//Lee el archivo
		try{
			//Lee la linea de referencia
			String line = in.readLine();

			//Empieza desde la primera linea de contenido
			line = in.readLine();
			while(!line.equals(null)){
				line.trim();
				String content[] = line.split(",");
				for(int i=0; i<content.length; i++){
					content[i] = content[i].trim();
				}

				if(content.length != 0){
					//Se crea un objeto a partir del archivo para ser agregado a la lista
					VOTrip actual = new VOTrip(content[0], content[1], content[2], content[3], content[4], 
							content[5], content[6], content[7], Integer.parseInt(content[8]), 
							Integer.parseInt(content[9]));
					trips.add(actual);
				}

				line = in.readLine();
			}			
		}
		catch(IOException e){
			System.out.println("No se pudo leer el archivo");
		}

		//Cierra el lector
		try{
			in.close();
		}
		catch(IOException e){
			System.out.println("No se pudo cerrar el lector de archivos");
			return;
		}
	}

	@Override
	public void loadStopTimes(String stopTimesFile)
	{

		// TODO Auto-generated method stub

		stopTimes= new DoubleLinkedList<VOStopTimes>();
		FileReader lector;
		BufferedReader in;
		String linea = null ;
		Calendar hora= Calendar.getInstance();
		try {

			lector = new FileReader(stopTimesFile);
			in = new BufferedReader(lector);
			linea = in.readLine();

			while(linea != null)
			{

				String[] datos = linea.split(",");
				int trip_id =Integer.parseInt( datos[0]);

				String[] arrival_time =(datos[1].split(":"));
				int horaA= Integer.parseInt(arrival_time[0]);
				int minA= Integer.parseInt(arrival_time[1]);
				int segA= Integer.parseInt(arrival_time[2]);
				//hora.set(Calendar.HOUR, Integer.parseInt(arrival_time[0]));
				//hora.set(Calendar.MINUTE, Integer.parseInt(arrival_time[1]));
				//hora.set(Calendar.SECOND, Integer.parseInt(arrival_time[2]));

				String[] departure_time =(datos[2].split(":"));
				int horaD= Integer.parseInt(departure_time[0]);
				int minD= Integer.parseInt(departure_time[1]);
				int segD= Integer.parseInt(departure_time[2]);
				//hora.set(Calendar.HOUR, Integer.parseInt(departure_time[0]));
				//hora.set(Calendar.MINUTE, Integer.parseInt(departure_time[1]));
				//hora.set(Calendar.SECOND, Integer.parseInt(departure_time[2]));

				int stop_id= Integer.parseInt( datos[3]);
				int stop_sequence = Integer.parseInt( datos[4]);
				String stop_headsign = datos[5];
				int pickup_type= Integer.parseInt( datos[6]);
				int drop_off_type=Integer.parseInt( datos[7]);
				double shape_dist_traveled=Double.parseDouble(datos[8]);
				
				//Se crea un objeto a partir del archivo para ser agregado a la lista
				VOStopTimes temp = new VOStopTimes(trip_id, horaA, minA, segA, horaD, minD, segD, stop_id, stop_sequence, stop_headsign, drop_off_type, shape_dist_traveled);
				stopTimes.add(temp);
			
		
				linea = in.readLine();
			}
			in.close();
			lector.close();
		}
		
		catch (Exception e) 
		{
			//throw new Exception("error");
		}

	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		stops = new RingList<VOStop>();
		FileReader lector;
		BufferedReader in;
		String linea = null ;
		try {

			lector = new FileReader(stopsFile);
			in = new BufferedReader(lector);
			linea = in.readLine();

			while(linea != null)
			{

				String[] datos = linea.split(",");
				int trip_id =Integer.parseInt( datos[0]);
				int stop_code= Integer.parseInt( datos[1]);
				String stopName= ( datos[3]);
				String stop_desc []= datos[4].split("@");
				String calle= stop_desc[0];
				String avenida= stop_desc[1];
				double stop_lat= Double.parseDouble(datos[5]);
				double stop_lon= Double.parseDouble(datos[6]);
				String zone_id []= datos[7].split(" ");
				String letra= zone_id[0];
				int num= Integer.parseInt(zone_id[1]);
				String stop_url = datos[8];
				
				String location_type= datos[9];
				int parent_station= Integer.parseInt(datos[10]);
					
				VOStop nueva= new VOStop(trip_id, stop_code, stopName, calle, avenida, stop_lat, stop_lon, letra,num,stop_url, location_type, parent_station);
				
				try 
				{
					stops.add(nueva);
				} 
				catch (Exception e) 
				{
					// Se ignora y se continua con el sigueinte
				}
				linea = in.readLine();
			}
			in.close();
			lector.close();
		}
		catch (Exception e) 
		{
			
		}

	}

	/**
	 * 1. Conseguir el id de la parada para el nombre que entra por par�metro en stops
	 *		1.1 B�squeda binaria (si la lista stops est� organizada por nombre)
	 *		1.2 Si no se encuentra el nombre dado por par�metro se retorna null
	 *	2. Conseguir el id del viaje para el id de la parada(recuperado en 1.) en stopTimes
	 *		2.1 B�squeda binaria (si la lista stop_times est� ordenada por id_stop
	 *	3. Conseguir el id de la ruta para el id del viaje (recuperado en 2.) en trips
	 *		3.1 B�squeda binaria (si la lista trips est� ordenada por id_trip)
	 *		3.2 Verificar que la ruta que se vaya a agregar todav�a no exista en la lista de rutas
	 *	4. Buscar con los id de ruta, los objetos de tipo VORoute que tengan esos id
	 * 
	 */
	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		
		
		//1.
		boolean continuar = true;
		String stopId = "no esta";
		for(int i=0; i<stops.getSize() && continuar; i++){
			if(stopName.equalsIgnoreCase(stops.getElement(i).getName())){
				stopId = String.valueOf(stops.getElement(i).id());
			}
		}
		
		//1.2
		if(stopId.equalsIgnoreCase("no esta")){
			System.out.println("No se encontr� la parada con el nombre dado");
			return null;
		}
		
		//2.
		DoubleLinkedList<String> tripsId = new DoubleLinkedList<String>();
		for(VOStopTimes actual: stopTimes){
			if(!tripsId.yaExiste(actual.getTripId()) && actual.getStopId().equalsIgnoreCase(stopId)){
				tripsId.add(actual.getTripId());
			}
		}
		
		//3.
		DoubleLinkedList<String> routesId = new DoubleLinkedList<String>();
		for(VOTrip actual: trips){
			//Va a verificar que el id de ruta del trip actual no exista y que el id del trip actual exista
			//dentro de la lista tripsId (Referirse al punto 2. de este m�todo)
			if(!routesId.yaExiste(actual.routeId()) && tripsId.yaExiste(actual.id())){
				routesId.add(actual.routeId());
			}
		}
		
		//4.
		IList<VORoute> routesBuscadas = new DoubleLinkedList<VORoute>();
		for(VORoute actual: routes){
			//Va a verificar que la ruta actual no exista y que el id de la ruta actual exista
			//dentro de la lista routesId (Referirse al punto 3. de este m�todo)
			if(!((DoubleLinkedList<VORoute>)routesBuscadas).yaExiste(actual) && routesId.yaExiste(actual.id())){
				routesBuscadas.add(actual);
			}
		}
		
		return routesBuscadas;
	}

	
	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {

		String stopId ="";

		if(stopId.equalsIgnoreCase("error")){
			System.out.println("error");
			return null;
		}
	
		//String direccion= "lat y lon";
		for(int i=0; i<stops.getSize() ; i++){
			if(routeName.equalsIgnoreCase(stops.getElement(i).getName())){
				stopId = String.valueOf(stops.getElement(i).id());
			}
		}

		DoubleLinkedList<String> tripsId = new DoubleLinkedList<String>();
		for(VOStopTimes actual: stopTimes){
			if(!tripsId.yaExiste(actual.getTripId()) && actual.getStopId().equalsIgnoreCase(stopId) && actual.getStopId().equals(Integer.parseInt(direction))){
				tripsId.add(actual.getStopId());
			}
		}


		IList<VOStop> buscar = new DoubleLinkedList<VOStop>();
		DoubleLinkedList<String> routesId = new DoubleLinkedList<String>();
		for(VORoute actual: routes){

			if(!((DoubleLinkedList<VOStop>)buscar).yaExiste(null) && routesId.yaExiste(actual.id())){
				routes.add(actual);
			}

		}
		return buscar;

	}

}
