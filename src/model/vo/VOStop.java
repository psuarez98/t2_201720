package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop<T extends Comparable<T>> {
	
	private int id;
	private int stop_code;
	private String stop_Name;
	private String stop_desc1;
	private String stop_desc2;
	private double stop_lat;
	private double stop_lon;
	private String zone_id1;
	private int zone_id2;
	private String stop_url;
	private String location_type;
	private int parent_station;
	
	
	public VOStop(int pId, int pStop_code, String pStop_name, String pStop_desc1, String pStop_desc2, double pStopLat,double pStopLon, String pZoneid1, int pZoneid2,String pUrl, String pLocTime,int pPsituation)
	{
		id=pId;
		stop_code=pStop_code;
		stop_Name=pStop_name;
		stop_desc1= pStop_desc1;
		stop_desc2= pStop_desc2;
		stop_lat=pStopLat;
		stop_lon= pStopLon;
		zone_id1= pZoneid1;
		zone_id2= pZoneid2;

		stop_url= pUrl;
		location_type= pLocTime;
		parent_station=pPsituation;
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getStop_code() {
		return stop_code;
	}


	public void setStop_code(int stop_code) {
		this.stop_code = stop_code;
	}


	public String getStop_Name() {
		return stop_Name;
	}


	public void setStop_Name(String stop_Name) {
		this.stop_Name = stop_Name;
	}


	public String getStop_desc1() {
		return stop_desc1;
	}


	public void setStop_desc1(String stop_desc1) {
		this.stop_desc1 = stop_desc1;
	}


	public String getStop_desc2() {
		return stop_desc2;
	}


	public void setStop_desc2(String stop_desc2) {
		this.stop_desc2 = stop_desc2;
	}


	public double getStop_lat() {
		return stop_lat;
	}


	public void setStop_lat(double stop_lat) {
		this.stop_lat = stop_lat;
	}


	public double getStop_lon() {
		return stop_lon;
	}


	public void setStop_lon(double stop_lon) {
		this.stop_lon = stop_lon;
	}


	public String getZone_id1() {
		return zone_id1;
	}


	public void setZone_id1(String zone_id1) {
		this.zone_id1 = zone_id1;
	}


	public int getZone_id2() {
		return zone_id2;
	}


	public void setZone_id2(int zone_id2) {
		this.zone_id2 = zone_id2;
	}


	public String getStop_url() {
		return stop_url;
	}


	public void setStop_url(String stop_url) {
		this.stop_url = stop_url;
	}


	public String getLocation_type() {
		return location_type;
	}


	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}


	public int getParent_station() {
		return parent_station;
	}


	public void setParent_station(int parent_station) {
		this.parent_station = parent_station;
	}


}
