package model.vo;

import java.util.Calendar;

public class VOStopTimes 
{
	private int trip_id;
	private int arrivalTime_hora;
	private int arrivalTime_minuto;
	private int arrivalTime_segundo;
	private int departureTime_hora;
	private int departureTime_minuto;
	private int departureTime_segundo;
	private int stopID;
	private int headSign;
	private String pickup_type;
	private int drop_off_type ;
	private double shape_dist_traveled;
	
	public VOStopTimes(int pId, int pHoraA, int pMinutoA,int pSegundoA, int pHoraD, int pMinutoD, int pSegundoD,int pStopID, int pHeadSign, String pPickup, int pDropOff,double pShape )
	{
		trip_id=pId;
		arrivalTime_hora=pHoraA;
		arrivalTime_minuto=pMinutoA;
		arrivalTime_segundo=pSegundoA;
		departureTime_hora= pHoraD;
		departureTime_minuto=pMinutoD;
		departureTime_segundo= pSegundoD;
		stopID=pStopID;
		headSign= pHeadSign;
		pickup_type=pPickup;
		drop_off_type= pDropOff;
		shape_dist_traveled=pShape;
	}
	
	public String getTripId(){
		return String.valueOf(trip_id);
	}
	
	public String getStopId(){
		return String.valueOf(stopID);
	}
}
