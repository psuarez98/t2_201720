package model.vo;

/**
 * Representation of a route object
 */
public class VOTrip {

	//---------------------------------------------
	//Atributos (Definidos basandonos en el archivo routes.txt)
	//---------------------------------------------
	
	private String routeId;
	private String serviceId;
	private String tripId;
	private String headsign;
	private String shortName;
	private String dirId;
	private String blockId;
	private String shapeId;
	private boolean wheelchairAccess;
	private boolean bikesAllowed;
	
	
	public VOTrip(String pRId, String pSId, String pTId, String pHS, String pSName, String pDir, String pBId, 
			String pSHId, int pWheelChair, int pBikes){
		routeId = pRId;
		serviceId = pSId;
		tripId = pTId;
		headsign = pHS;
		shortName = pSName;
		dirId = pDir;
		blockId = pBId;
		shapeId = pSHId;
		
		if(pWheelChair == 0)
			wheelchairAccess = false;
		else
			wheelchairAccess = true;
		
		if(pBikes == 0)
			bikesAllowed = false;
		else
			bikesAllowed = true;
		
	}
	
	public String routeId(){
		return routeId;
	}
	
	/**
	 * @return id - Route's id number
	 */
	public String id() {
		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		return shortName;
	}
	

}
