package model.vo;

/**
 * Representation of a route object
 */
public class VORoute {

	//---------------------------------------------
	//Atributos (Definidos basandonos en el archivo routes.txt)
	//---------------------------------------------
	
	private String routeId;
	private String agencyId;
	private String shortName;
	private String longName;
	private String description;
	private int type;
	private String url;
	
	
	public VORoute(String pRId, String pAId, String pSName, String pLName, String pDesc, int pType, String pUrl){
		routeId = pRId;
		agencyId = pAId;
		shortName = pSName;
		longName = pLName;
		description = pDesc;
		type = pType;
		url = pUrl;		
	}
	
	/**
	 * @return id - Route's id number
	 */
	public String id() {
		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		return shortName;
	}
	

}
