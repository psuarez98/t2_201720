package model.data_structures;

public class Node<T>{
	
	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	
	/**
	 * Elemento que contiene el nodo
	 */
	private T element;
	
	/**
	 * Siguiente nodo al que est� encadenado	
	 */
	private Node<T> next;
	
	/**
	 * Anterior nodo al que est� encadenado
	 */	
	private Node<T> prev;
	
	/**
	 * Identificador del nodo. En cualquier lista, los id son �nicos
	 * La lista es la que se va a encargar de que se cumpla esta condici�n
	 */
	private String id;

	public Node anterior;
	
	
	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------
	
	/**
	 * Constructor para un unico nodo
	 * @param pElement Elemento que va a contener el nodo
	 * @param pId Identificador del Nodo
	 */
	public Node(T pElement, String pId){
		
		element = pElement;
		
		id = pId;
		
		next = null;
		
		prev = null;
		
	}
	
	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------
	
	/**
	 * @return Retorna el elemento guardado en el nodo
	 */
	public T darElemento(){
		return element;
	}
	
	/**
	 * @return Retorna el identificador del nodo
	 */
	public String darId(){
		return id;
	}
	
	/**
	 * @return Retorna el siguiente nodo
	 */
	public Node<T> darSig(){
		return next;
	}
	
	/**
	 * @return Retorna el anterior nodo
	 */
	public Node<T> darAnt(){
		return prev;
	}
	
	/**
	 * Cambia el nodo anterior
	 * @param pNodo Nuevo nodo anterior
	 * @return True si la operaci�n funcion�. False en caso contrario
	 */
	public boolean cambiarAnt(Node<T> pNodo){
		try{
			prev = pNodo;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * Cambia el nodo siguiente
	 * @param pNodo Nuevo nodo siguiente
	 * @return True si funcion�. False en caso contrario
	 */
	public boolean cambiarSig(Node<T> pNodo){
		try{
			next = pNodo;
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
