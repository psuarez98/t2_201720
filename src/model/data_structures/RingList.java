package model.data_structures;

import java.util.Iterator;

public class RingList<T> implements IList<T>{
	
	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	
	/**
	 * Primer nodo de la lista
	 */
	private Node<T> first;
	
	/**
	 * Posicion actual de la lista. Siempre va a estar entre 0 y size-1
	 */
	private int actual;
	
	/**
	 * Tama�o total de la lista
	 */
	private int size;
	
	
	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------
	
	public RingList(){
		first = null;
		size = 0;
		actual = 0;
	}
	
	
	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------
	
	/**
	 * Agrega un elemento a la lista en la posicion siguiente a la actual
	 * @param elem Elemento que va a agregar
	 */
	public void add(T elem){
		Node<T> agregar = new Node<T>(elem, String.valueOf(elem));
		if(yaExiste(String.valueOf(elem))){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(size == 0){
			first = agregar;
			first.cambiarAnt(first);
			first.cambiarSig(first);
		}
		else{
			Node<T> nActual = first;
			for(int i=0; i<=actual; i++){
				if(i != actual)
					nActual = nActual.darSig();
				else{
					agregar.cambiarSig(nActual.darSig());
					nActual.darSig().cambiarAnt(agregar);
					nActual.cambiarSig(agregar);
					agregar.cambiarAnt(nActual);
				}
			}
		}
		size ++;
		actual = (actual+1)%size;
	}
	
	/**
	 * Agrega un elemento al final de la lista	
	 * @param elem Elemento que va a agregar
	 */
	public void addAtEnd(T elem){
		Node<T> agregar = new Node<T>(elem, String.valueOf(elem));
		if(yaExiste(String.valueOf(elem))){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(size == 0){
			first = agregar;
			first.cambiarSig(agregar);
			first.cambiarAnt(agregar);
		}
		else{
			Node<T> nActual = first;
			for(int i=0; i<size; i++){
				if(nActual.darSig().darId().equalsIgnoreCase(first.darId())){
					agregar.cambiarSig(first);
					first.cambiarAnt(first);
					nActual.cambiarSig(agregar);
					agregar.cambiarAnt(nActual);
				}
				else
					nActual = nActual.darSig();
			}
		}
		size ++;
		actual = (actual+1)%size;
	}
	
	/**
	 * Da el elemento en la posici�n i
	 * @param i Posici�n del elemento a recuperar.
	 * @return Elemento recuperado de la posici�n i. Retorna null si no hay elementos en la lista
	 */
	public T getElement(int i){
		if(size == 0)
			return null;
		else if(size == 1)
			return first.darElemento();
		else{
			Node<T> nActual = first;
			int pos = i%size;
			for(int j=0; j<=pos; j++){
				if(j != pos)
					nActual = nActual.darSig();
				else
					break;
			}
			return nActual.darElemento();
		}	
	}
	
	/**
	 * Da el elemento en la posici�n actual
	 * @return Elemento en la posici�n actual. Si no hay elementos retorna null
	 */
	public T getCurrentElement(){
		if(size == 0)
			return null;
		else if(size == 1)
			return first.darElemento();
		else{
			Node<T> nActual = first;
			for(int i=0; i<=actual; i++){
				if(i!=actual)
					nActual = nActual.darSig();
				else
					break;
			}
			return nActual.darElemento();
		}
	}
	
	/**
	 * Da el tama�o total de la lista
	 * @return Tama�o de la lista
	 */
	public Integer getSize(){
		return size;
	}
	
	/**
	 * Borra toda la lista
	 * @return True si funcion�. False en caso contrario
	 */
	public boolean delete(){
		first = null;
		size=0;
		actual = 0;
		return true;
	}
	
	/**
	 * Borra el elemento de la posici�n k
	 * @param k posici�n del elemento a borrar
	 * @return True si funcion�. False en caso contrario
	 */
	public boolean deleteAtK(int k){
		if(size == 0)
			return false;
		else if(size == 1){
			first = null;
			size --;
			actual = 0;
			return true;
		}
		else{
			int pos = k%size;
			Node<T> nActual = first;
			for(int i=0; i<=pos; i++){
				if(i != pos)
					nActual = nActual.darSig();
				else
					break;
			}
			Node<T> ant = nActual.darAnt();
			Node<T> sig = nActual.darSig();
			nActual.darAnt().cambiarSig(sig);
			nActual.darSig().cambiarAnt(ant);
			size --;
			actual = (actual-1)%size;
			return true;
		}
	}
	
	/**
	 * Mueve la posici�n actual al siguiente elemento
	 */
	public void next(){
		actual = (actual+1)%size;
	}
	
	/**
	 * Mueve la posici�n actual al anterior elemento
	 */
	public void previous(){
		actual = (actual-1)%size;
	}
	
	/**
	 * Verifica si el id que entra por par�metro ya existe en la lista
	 * @param pId El identificador buscado
	 * @return True si ya existe dentro de la lista. False en caso contrario
	 */
	public boolean yaExiste(String pId){
		if(size == 0)
			return false;
		else{
			boolean existe = false;
			if(first.darId().equalsIgnoreCase(pId)){
				return true;
			}
			else{
				Node<T> nActual = first;
				for(int i=0; i<size && !existe; i++){
					if(nActual.darId().equalsIgnoreCase(pId))
						existe = true;
					else
						nActual = nActual.darSig();
				}
			}
			return existe;
		}
	}

	//TODO
	@Override
	public Iterator<T> iterator() {
		return null;
	}
	
}
