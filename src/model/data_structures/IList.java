package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, addAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {
	
	/**
	 * Agrega un elemento a la lista
	 * @param elem Elemento que va a agregar
	 * @return 
	 */
	void add(T elem);
	
	/**
	 * Agrega un elemento al final de la lista	
	 * @param elem Elemento que va a agregar
	 */
	void addAtEnd(T elem);
	
	
	
	/**
	 * Da el elemento en la posici�n i
	 * @param i Posici�n del elemento a recuperar
	 * @return Elemento recuperado de la posici�n i
	 */
	T getElement(int i);
	
	/**
	 * Da el elemento en la posici�n actual
	 * @return Elemento en la posici�n actual
	 */
	T getCurrentElement();
	
	/**
	 * Da el tama�o total de la lista
	 * @return Tama�o de la lista
	 */
	Integer getSize();
	
	/**
	 * Borra toda la lista
	 * @return True si funcion�. False en caso contrario
	 */
	boolean delete();
	
	/**
	 * Borra el elemento de la posici�n k
	 * @param k posici�n del elemento a borrar
	 * @return True si funcion�. False en caso contrario
	 */
	boolean deleteAtK(int k);
	
	/**
	 * Mueve la posici�n actual al siguiente elemento
	 */
	void next();
	
	/**
	 * Mueve la posici�n actual al anterior elemento
	 */
	void previous();

}
