package model.data_structures;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;


import java.util.List;

import model.vo.*;


public class DoubleLinkedList <T> implements IList<T> 
{

	private int tamanio;
	private int actual;
	private Node<T> primero;
	private Node<T> current;
	private  List<T> lista;
	private Node<T> ultimo;
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	public  DoubleLinkedList() {
		tamanio=0;
		primero=null;
		lista=null;
	}

	@Override
	

	public void add(T elem) {
		Node<T> nuevoNodo = new Node <T> (elem, String.valueOf(elem));
		if(yaExiste(elem))
		{
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		if(primero == null)
		{
			ultimo = nuevoNodo;
			primero=ultimo;
		}
		else
		{
			Node <T> current = new Node(getCurrentElement(),String.valueOf(getCurrentElement()) );
			Node <T> nextCurrent = current.darSig();
			nuevoNodo.cambiarAnt(current.darSig());
			nuevoNodo.cambiarAnt(current);
			current.cambiarSig(nuevoNodo);
			nextCurrent.cambiarAnt(nuevoNodo);
		}
		tamanio++;

	}


	public boolean yaExiste(T elemento) {

		boolean existe = false;
		if(tamanio == 0)

			return false;
		else{

			if(primero.darElemento().equals(elemento))
				return true;

			else{
				Node<T> nActual = primero;
				for(int i=0; i<tamanio && !existe; i++){
					if(nActual.darElemento().equals(elemento))
						existe = true;
				}
			}
			return existe;
		}
	}

	@Override
	public void addAtEnd(T elem) {
		// TODO Auto-generated method stub
		Node<T> agregar = new Node<T>(elem, String.valueOf(elem));
		if(yaExiste(elem)){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(tamanio == 0){
			ultimo=agregar;
			primero=ultimo;
		}
		else{
			ultimo.cambiarSig(agregar);
			agregar.cambiarSig(null);
			ultimo=agregar;

		}
		tamanio ++;

	}

	@Override
	public T getElement(int i) {
		// TODO Auto-generated method stub
		T ans = null;
//		if(lista.size() == 0)
//			return null;
		if(i<lista.size())
		{
			ans= lista.get(i);
		}
		return ans;
	}

	@Override
	public T getCurrentElement() {
		return getElement(actual);
	}

	@Override
	public Integer getSize() {
		return lista.size();
	}

	
	public Node<T> obtnerNodoK(int k)
	{
		Node<T> nodoBucado= null;
		T elem = getElement(k);

			Node<T> primer=primero;
			while(primer!=null)
			{
				if(primer.darElemento().equals(elem))
				nodoBucado=primer;
				
				primer=primer.darSig();
			}
			return nodoBucado;
			
	}
	
	public boolean deleteAtK(int k) {
		
		Node <T> nActual=obtnerNodoK(k);
		if(nActual.equals(null))
			return false;
		else{
			
		Node <T> anteriorN = nActual.darAnt();
		Node <T> siguienteN = nActual.darSig();
		
		anteriorN.cambiarSig(siguienteN);
		siguienteN.cambiarAnt(anteriorN);
		nActual.cambiarAnt(null);
		nActual.cambiarSig(null);
		return true;
		}
	}

	@Override
	public void next() {
		actual+=1;


	}

	@Override
	public void previous() {
		actual-=1;

	}
	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		primero = null;
		tamanio=0;
		actual = 0;
		return true;
	}
	public Object darPrimerElemento() {
		// TODO Auto-generated method stub
		return primero;
	}
	public Object darTamanio() {
		// TODO Auto-generated method stub
		return tamanio;
	}
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(lista.size()==0)
		{
			return true;
		}
		else
			return false;
	}


}
