package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.*;

public class DoubleLinkedListTest extends TestCase{
	
	//-----------------------------------------------------
	//Atributos
	//-----------------------------------------------------
	
	private DoubleLinkedList<Integer> lista;
	
	
	//-----------------------------------------------------
	//Esecnarios
	//-----------------------------------------------------
	public void setUp(){
		lista = new DoubleLinkedList<Integer>();
	}
	
	//-----------------------------------------------------
	//Tests
	//-----------------------------------------------------
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un elemento
	 * Caso 3: La lista tiene varios elementos
	 */
	public void testAdd(){
		setUp();
		
		lista.add(5);
		
		assertEquals("El elemento no fue agregado correctamente", 1, lista.darTamanio());
		
		lista.add(3);
		
		assertEquals("El elemento no fue agregado correctamente", 2, lista.darTamanio());
		
		lista.add(9);
		
		assertEquals("El elemento no fue agregado correctamente", 3, lista.darTamanio());
		
	}
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un elemento
	 * Caso 3: La lista tiene varios elementos
	 */
	public void testDelete(){
		setUp();
		
		lista.delete();
		
		assertNull(lista.darPrimerElemento());
		
		lista.add(5);
		lista.delete();
		assertEquals("El elemento no fue eliminado correctamente", 0, lista.darTamanio());
		
		lista.add(5);
		lista.add(6);
		lista.add(7);
		int tamAnt = (int) lista.darTamanio();
		lista.delete();
		assertEquals("El elemento no fue eliminado correctamente", tamAnt-1, lista.darTamanio());
	}
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista no est� vac�a
	 */
	public void testIsEmpty(){
		setUp();
		assertTrue(lista.isEmpty());
		
		lista.add(5);
		assertFalse(lista.isEmpty());
	}
}
