package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.*;

public class RingListTest extends TestCase {
	
	//-------------------------------------------
	//Atributos
	//-------------------------------------------
	
	private RingList<Integer> lista;
	
	//-------------------------------------------
	//setUP
	//-------------------------------------------
	
	/**
	 * Inicializa la lista circular como vac�a
	 */
	public void setUp(){
		lista = new RingList<Integer>();
	}
	
	/**
	 * Agrega a la lista un solo elemento
	 */
	public void setUpEscenario2()throws Exception{
		setUp();
		lista.add(5);
	}
	
	/**
	 * Agrega a la lista 5 elementos
	 */
	public void setUpEscenario3() throws Exception{
		setUp();
		setUpEscenario2();
		lista.add(6);
		lista.add(7);
		lista.add(8);
		lista.add(9);
	}
	//-------------------------------------------
	//Tests
	//-------------------------------------------
	
	/**
	 * Verifica el m�todo add(T, String)
	 */
	public void testAdd(){
		setUp();
		try{
			setUpEscenario3();
		}
		catch(Exception e){
			fail("No deber�a lanzar la excepci�n");
			e.printStackTrace();
		}
	}
	
	/**
	 * Verifica el m�todo addAtEnd(T, String)
	 */
	public void testAddAtEnd(){
		setUp();
		try{
			setUpEscenario3();
			 
		}
		catch(Exception e){
			fail("El m�todo add(T, String) no deber�a lanzar excepci�n");
			e.printStackTrace();
		}
		try{
			lista.addAtEnd(10);
		}
		catch(Exception e){
			fail("El m�todo addAtEnd(T, String) no deber�a lannzar excepci�n");
			e.printStackTrace();
		}
		
		assertEquals("El elemento no se agreg� correctamente al final de la lsita", Integer.valueOf(10), lista.getElement(lista.getSize()-1));
	}
	
	/**
	 * Verifica el m�todo delete()
	 */
	public void testDelete(){
		setUp();
		try{
			setUpEscenario3();
		}
		catch(Exception e){
			fail("El m�todo add(T, String) no deber�a lanzar excepci�n");
			e.printStackTrace();
		}
		assertTrue("Debi� devolver true",lista.delete());
		lista.delete();
		assertEquals("El tama�o de la lista deber�a ser 0", Integer.valueOf(0), lista.getSize());
	}
	
	/**
	 * Verifica el m�todo deleteAtK(int)
	 */
	public void testDeletAtK(){
		setUp();
		try{
			setUpEscenario3();
		}
		catch(Exception e){
			fail("El m�todo add(T, String) no deber�a lanzar excepci�n");
			e.printStackTrace();
		}
		assertTrue("Debi� devolver true", lista.deleteAtK(2));
		assertFalse("No debi� encontrar el elemento que fue eliminado", lista.yaExiste("7"));
	}
	
	/**
	 * Verifica el m�todo yaExiste(String)
	 * Caso 1: El elemento ya existe
	 * Caso 2: El elemento no existe
	 */
	public void testYaExiste(){
		setUp();
		try{
			setUpEscenario3();
		}
		catch(Exception e){
			fail("El m�todo add(T, String) no deber�a lanzar excepci�n");
			e.printStackTrace();
		}
		
		assertTrue("Debi� encontrar el elemento", lista.yaExiste("7"));
		assertFalse("No debi� encontrar el elemento", lista.yaExiste("10000"));
	}
	
}
